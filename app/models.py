from django.db import models


class Transactions(models.Model):
    TRANSACTION_CHOICES = (('p', 'purchase'),
                           ('r', 'refund'),
                           ('w', 'withdrawal'))
    transaction_type = models.CharField(max_length=9,
                                        choices=TRANSACTION_CHOICES,
                                        default='purchase')
    user_id = models.IntegerField()
    transaction_id = models.IntegerField(primary_key=True)
    amount = models.DecimalField(decimal_places=2, max_digits=100)
    created = models.DateTimeField(auto_created=True)
