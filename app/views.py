from django.db.models import Sum, Count
from rest_framework import status
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.views import APIView
from app.models import Transactions
from app.serializers import TransactionsSerializer


class ResponsePagination(PageNumberPagination):
    page_query_param = 'p'
    page_size = 3
    page_size_query_param = 'page_size'
    max_page_size = 3


class Index(APIView):
    def get(self, request):
        try:
            return Response({"Health": "OK"}, status.HTTP_200_OK)
        except Exception as e:
            return Response(status=status.HTTP_404_NOT_FOUND)


class Customer(APIView):
    def get(self, request, cust_id):
        """
        user authentication would be like:
        user = authenticate(username=username, password=password)
        login(request, user)
        if user:
        """
        try:
            user_trx = Transactions.objects.values().filter(user_id=cust_id)
            trx_no = user_trx.aggregate(Total=Count('amount'))
            balance = user_trx.aggregate(Total=Sum('amount'))
            paginator = ResponsePagination()
            if len(user_trx) > 0:
                return Response({'User id': cust_id,
                                 'Number of transactions': trx_no,
                                 'Balance': balance,
                                 'Transactions': user_trx})
            else:
                return Response({"Error": "User does not exist."}, status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def post(self, request, cust_id):
        request = request.data
        data_json = {
            "created": request['created'],
            "transaction_type": request['transaction_type'],
            "user_id": cust_id,
            "transaction_id": request['transaction_id'],
            "amount": request['amount']
        }
        serializer = TransactionsSerializer(data=data_json)
        print(request)
        print(data_json)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

